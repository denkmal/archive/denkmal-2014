denkmal.org 2014-2018
=====================

Historical repo of denkmal.org.
- **Functionality**: List of daily events, favorite venues, audio files for some events, chat for each venue (*Denkmal Now*).
- **Technology**: PHP backend based on [CM](https://github.com/cargomedia/cm) with server-side rendered frontend.

---
![Screenshot 1](docs/screenshot1.png)
---
![Screenshot 2](docs/screenshot2.png)
---
![Screenshot 3](docs/screenshot3.png)
